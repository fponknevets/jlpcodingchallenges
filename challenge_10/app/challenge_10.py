from dataclasses import dataclass
from random import seed
from random import randint

@dataclass()
class Location:
    position: int = 1

    def move(self, start, amount):
        if start + amount > 40:
            return start + amount - 40
        else:
            return start + amount


@dataclass()
class Player:

    name: str
    location: Location = Location()
    board_location: int = location.position

    def move(self, amount):
        self.board_location = self.location.move(self.board_location, amount)


@dataclass()
class Dice:

    def __init__(self):
        self.die_1 = randint(1, 6)
        self.die_2 = randint(1, 6)
        self.total = self.die_1 + self.die_2


def passed_go(start, end):
    return start <= 40 and end > 1