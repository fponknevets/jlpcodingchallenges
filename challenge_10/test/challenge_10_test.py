import unittest
from challenge_10.app.challenge_10 import Player
from challenge_10.app.challenge_10 import Dice
from challenge_10.app.challenge_10 import passed_go


class TestPlayer(unittest.TestCase):

    def test_can_create_player_with_name(self):
        player = Player("Steven")
        self.assertEqual(player.name, "Steven")

    def test_cannot_create_player_without_name(self):
        with self.assertRaises(TypeError):
            player = Player()

    def test_created_player_has_location_zero(self):
        player = Player("Steven")
        self.assertEqual(player.board_location, 1)

    def test_can_move_player_fewer_spaces_than_distance_to_go(self):
        player = Player("Steven")
        player.move(10)
        self.assertEqual(player.board_location, 11)

    def test_can_move_player_more_spaces_than_distance_to_go(self):
        player = Player("Steven")
        player.move(12)
        player.move(12)
        player.move(12)
        player.move(10)
        self.assertEqual(player.board_location, 7)

    def test_dice_returns_two_ints_between_one_and_six(self):
        dice = Dice()
        die_1 = dice.die_1
        die_2 = dice.die_2
        self.assertTrue(type(die_1) == int)
        self.assertTrue(type(die_2) == int)
        self.assertTrue(1 <= die_1 <= 6)
        self.assertTrue(1 <= die_2 <= 6)

    def test_the_randomness_of_our_dice(self):
        dice1_roll_register = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
        dice2_roll_register = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
        dice_total_roll_register = {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10:0, 11: 0, 12: 0}
        for _ in range(1, 60001):
            dice = Dice()
            die_1 = dice.die_1
            die_2 = dice.die_2
            dice1_roll_register[die_1] = dice1_roll_register.get(die_1) + 1
            dice2_roll_register[die_2] = dice2_roll_register.get(die_2) + 1
        for side in range(1, 7):
            self.assertTrue(9800 < dice1_roll_register[side] < 10200)
            self.assertTrue(9800 < dice2_roll_register[side] < 10200)

    def test_correctly_adjudicates_passing_go(self):
        self.assertTrue(passed_go(39, 2))
        self.assertFalse(passed_go(30, 1))

    def test_can_pass_dice_result_to_move_player(self):
        player = Player(name="John")
        dice = Dice()
        player.move(dice.total)
        self.assertTrue(player.board_location == dice.total + 1)


if __name__ == "__main__":
    unittest.main()
