import unittest

from challenge_8.app.challenge_8 import Location
# from challenge_8.app.challenge_8 import location_types


class LocationTests(unittest.TestCase):

    def test_can_create_free_parking(self):
        free_parking = Location('fp')
        self.assertEqual(free_parking.name, "Free Parking")
        self.assertEqual(free_parking.description, "Free Parking")
        self.assertFalse(free_parking.can_be_purchased)
        self.assertFalse(free_parking.charges_rent)

    def test_can_create_go(self):
        go = Location('go')
        self.assertEqual(go.name, "Go")
        self.assertEqual(go.description, "Go")
        self.assertFalse(go.can_be_purchased)
        self.assertFalse(go.charges_rent)

    def test_can_create_a_warehouse_site(self):
        magna_park = Location('fw', "Magna Park")
        self.assertEqual(magna_park.name, "Magna Park")
        self.assertEqual(magna_park.description, "Factory / Warehouse")
        self.assertTrue(magna_park.can_be_purchased)
        self.assertEqual(magna_park.price, 100)
        self.assertTrue(magna_park.charges_rent, True)
        self.assertEqual(magna_park.rent, 20)

    def test_can_create_a_retail_site(self):
        john_lewis_oxford_street = Location('rs', "John Lewis Oxford Street", 800, 140, "Purple")
        self.assertEqual(john_lewis_oxford_street.name, "John Lewis Oxford Street")
        self.assertEqual(john_lewis_oxford_street.description, "Retail Site")
        self.assertTrue(john_lewis_oxford_street.can_be_purchased)
        self.assertEqual(john_lewis_oxford_street.price, 800)
        self.assertTrue(john_lewis_oxford_street.charges_rent, True)
        self.assertEqual(john_lewis_oxford_street.rent, 140)
        self.assertEqual(john_lewis_oxford_street.group, "Purple")

    def test_creating_a_retail_site_without_a_name_raises_exception(self):
        with self.assertRaises(RuntimeError):
            anonymous_retail_site = Location('rs', price=100, rent=20)

    def test_creating_free_parking_with_rent_raises_exception(self):
        with self.assertRaises(RuntimeError):
            free_parking = Location('fp', "Free Parking", rent=40)

    def test_creating_free_parking_with_purchase_price_raises_exception(self):
        with self.assertRaises(RuntimeError):
            free_parking = Location('fp', "Free Parking", price=400)

    def test_cannot_create_invalid_location_type(self):
        with self.assertRaises(RuntimeError):
            invalid_location = Location('zz')


if __name__ == "__main__":
    unittest.main()

