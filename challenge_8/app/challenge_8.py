from dataclasses import dataclass


DESCRIPTION = 0
CAN_BE_PURCHASED = 1
PRICE = 2
CHARGES_RENT = 3
RENT = 4


location_types = {"fp": ["Free Parking", False, 0, False, 0],
                  "go": ["Go", False, 0, False, 0],
                  "fw": ["Factory / Warehouse", True, 100, True, 200],
                  "rs": ["Retail Site", True, 0, True, 0]}


@dataclass
class Location:

    def __init__(self, location_type, name=None, price=None, rent=None, group=None):

        # valid location type
        if location_type in location_types:
            self.location_type = location_type
        else:
            raise RuntimeError("Invalid location type specified.")

        # Free Parking and Go
        if self.location_type == 'fp' or self.location_type == 'go':
            if (name is not None) or (price is not None) or (rent is not None) or (group is not None):
                raise RuntimeError("Free Parking / Go should not have name nor price nor rent nor group.")

        # Factories and Warehouses
        if self.location_type == 'fw':
            if name is None:
                raise RuntimeError("Factories / Warehouses need a name.")

            if (price is not None) or (rent is not None) or (group is not None):
                raise RuntimeError("Factory / Warehouses all have a fixed price and fixed rent and no group.")
            price = 100
            rent = 20

        # Retail site
        if self.location_type == 'rs':
            if (name is None) or (price is None) or (rent is None) or (group is None):
                raise RuntimeError("Retail sites need a name, price, rent and group.")

        self.name = name if location_type in ('fw', 'rs') else location_types.get(location_type)[DESCRIPTION]
        self.description = location_types.get(location_type)[DESCRIPTION]
        self.price = price
        self.rent = rent
        self.group = group
        self.can_be_purchased = bool(self.price)
        self.charges_rent = bool(self.rent)