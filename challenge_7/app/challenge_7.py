
def number_is_even_and_less_than(yard_stick):

    def _number_is_even_and_less_than_yard_stick(number):
        try:
            float_number = float(number)
            return float_number % 2 == 0 and float_number < yard_stick
        except ValueError:
            return False
    return _number_is_even_and_less_than_yard_stick

