import unittest

from challenge_7.app.challenge_7 import number_is_even_and_less_than
from challenge_6.app.challenge_6 import my_filter


class MyEvenAndLessThanTests(unittest.TestCase):

    def test_8_is_even_and_less_than_10(self):
        this = 10
        eight = 8
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertTrue(number_is_even_and_less_than_10(eight))

    def test_9_is_even_and_less_than_10(self):
        this = 10
        nine = 5
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(nine))

    def test_10_is_even_and_less_than_10(self):
        this = 10
        ten = 10
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(ten))

    def test_11_is_even_and_less_than_10(self):
        this = 10
        eleven = 11
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(eleven))

    def test_12_is_even_and_less_than_10(self):
        this = 10
        twelve = 12
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(twelve))

    def test_a_is_even_and_less_than_10(self):
        this = 10
        char_a = 'a'
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(char_a))

    def test_abc_is_even_and_less_than_10(self):
        this = 10
        string_abc = 'abc'
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_10(string_abc))

    def test_filter_function_can_be_used_with_my_filter(self):
        this = 10
        int_list = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        number_is_even_and_less_than_10 = number_is_even_and_less_than(this)
        self.assertEqual(my_filter(int_list, number_is_even_and_less_than_10), [6, 8])

    def test_6point5_is_even_and_less_than_10(self):
        this = 10
        six_point_five = 6.5
        number_is_even_and_less_than_ten = number_is_even_and_less_than(this)
        self.assertFalse(number_is_even_and_less_than_ten(six_point_five))


if __name__ == "__main__":
    unittest.main()