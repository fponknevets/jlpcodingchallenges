import datetime
from dataclasses import dataclass

@dataclass()
class Player:

    def __init__(self, name):
        self.name = name


class GameLedger:

    def __init__(self):
        self.transactions = []

    def _record(self, source, tx_type, target, amount, notes=None):
        self.transactions.append((datetime.datetime.now(), source, tx_type, target, amount, notes))

    def bank2player(self, tx_type, target, amount):
        self._record("Bank", tx_type, target, amount)

    def rental_payment(self, source, target, amount):
        self._record(source, "Rent", target, amount)

    def purchase(self, source, amount, location):
        self._record(source, "Purchase", "Bank", amount, location)

    def build(self, source, amount, location):
        self._record(source, "Build", "Bank", amount, location)

