import unittest

from challenge_9.app.challenge_9 import GameLedger
from challenge_9.app.challenge_9 import Player


class GameLedgerTests(unittest.TestCase):

    def setUp(self):
        self.game_ledger = GameLedger()

    def test_ledger_can_be_created(self):
        self.assertTrue(self.game_ledger is not None)

    def test_ledger_has_record_of_financial_transactions(self):
        transax = self.game_ledger.transactions
        self.assertEqual(transax, [])

    def test_can_add_bank2player_transfer(self):
        self.player_1 = Player("Simon")
        self.game_ledger.bank2player("Transfer", self.player_1.name, 1000)
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][1], "Bank")
        self.assertEqual(transax[0][2], "Transfer")
        self.assertEqual(transax[0][3], "Simon")
        self.assertEqual(transax[0][4], 1000)

    def test_can_add_bank2player_fee(self):
        self.player_1 = Player("Simon")
        self.game_ledger.bank2player("Fee", self.player_1.name, 1000)
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][1], "Bank")
        self.assertEqual(transax[0][2], "Fee")
        self.assertEqual(transax[0][3], "Simon")
        self.assertEqual(transax[0][4], 1000)

    def test_can_add_player2player_rent(self):
        self.player_1 = Player("Adam")
        self.player_2 = Player("Simon")
        self.game_ledger.rental_payment(self.player_1.name, self.player_2.name, 100)
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][1], "Adam")
        self.assertEqual(transax[0][2], "Rent")
        self.assertEqual(transax[0][3], "Simon")
        self.assertEqual(transax[0][4], 100)

    def test_can_add_purchase(self):
        self.player_1 = Player("Simon")
        self.game_ledger.purchase(self.player_1.name, 200, "John Lewis Oxford Street")
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][1], "Simon")
        self.assertEqual(transax[0][2], "Purchase")
        self.assertEqual(transax[0][3], "Bank")
        self.assertEqual(transax[0][4], 200)
        self.assertEqual(transax[0][5], "John Lewis Oxford Street")

    def test_can_add_building_fee(self):
        self.player_1 = Player("Simon")
        self.game_ledger.build(self.player_1.name, 100, "Green Park")
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][1], "Simon")
        self.assertEqual(transax[0][2], "Build")
        self.assertEqual(transax[0][3], "Bank")
        self.assertEqual(transax[0][4], 100)
        self.assertEqual(transax[0][5], "Green Park")

    def test_can_add_multiple_entries(self):
        self.player_1 = Player("Simon")
        self.player_2 = Player("Adam")
        self.game_ledger.build(self.player_1.name, 100, "Green Park")
        self.game_ledger.purchase(self.player_1.name, 200, "John Lewis Oxford Street")
        self.game_ledger.rental_payment(self.player_2.name, self.player_1.name, 100)
        self.game_ledger.bank2player("Fee", self.player_1.name, 1000)
        transax = self.game_ledger.transactions
        self.assertEqual(transax[0][2], "Build")
        self.assertEqual(transax[0][4], 100)
        self.assertEqual(transax[1][2], "Purchase")
        self.assertEqual(transax[1][4], 200)
        self.assertEqual(transax[2][2], "Rent")
        self.assertEqual(transax[2][4], 100)
        self.assertEqual(transax[3][2], "Fee")
        self.assertEqual(transax[3][4], 1000)


if __name__ == "__main__":
    unittest.main()
