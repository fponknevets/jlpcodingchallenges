def get_grid_status(grid):

    BOARD_FULL = True
    whos_next = None

    VERTICAL = 0
    HORIZONTAL = 1
    DIAGONAL = 2

    RED = 0
    YELLOW = 1

    def _grid_to_2d_array(grid):
        c_4_board = []
        for line in grid:
            row=[]
            for column in line:
                row.append(column)
            c_4_board.append(row)
        return c_4_board

    def _get_board_params(c_4_board):
        return len(c_4_board[0]), len(c_4_board)

    def _seek_vertical(row, col, colour):
        for neighbour in range(1,4):
            if c_4_board[row + neighbour][col].lower() != colour.lower():
                return False
        return True

    def _seek_horizontal(row, col, colour):
        for neighbour in range(1,4):
            if c_4_board[row][col + neighbour].lower() != colour.lower():
                return False
        return True

    def _seek_left_diag(row, col, colour):
        for neighbour in range(1,4):
            if c_4_board[row + neighbour][col - neighbour].lower() != colour.lower():
                return False
        return True

    def _seek_right_diag(row, col, colour):
        for neighbour in range(1,4):
            if c_4_board[row + neighbour][col + neighbour].lower() != colour.lower():
                return False
        return True

    def _search_for_line(row, col, colour):

        potential_vertical = False
        potential_horizontal = False
        potential_left_diag = False
        potential_right_diag = False

        if row + 3 < BOARD_HEIGHT:
            potential_vertical = True
        if col + 3 < BOARD_WIDTH:
            potential_horizontal = True
        if (row + 3 < BOARD_HEIGHT) and (col - 3 >= 0):
            potential_left_diag = True
        if (row + 3 < BOARD_HEIGHT) and (col + 3 < BOARD_WIDTH):
            potential_right_diag = True

        if potential_vertical:
            if _seek_vertical(row, col, colour):
                return True

        if potential_horizontal:
            if _seek_horizontal(row, col, colour):
                return True

        if potential_left_diag:
            if _seek_left_diag(row, col, colour):
                return True

        if potential_right_diag:
            if _seek_right_diag(row, col, colour):
                return True

        return False

    c_4_board = _grid_to_2d_array(grid)
    BOARD_WIDTH, BOARD_HEIGHT = _get_board_params(c_4_board)
    whos_next = "Red"
    game_over = True

    for row_num, row in enumerate(grid):
        for col_num, col in enumerate(row):
            if col.upper() in ('R', 'Y'):
               if _search_for_line(row_num, col_num, col):
                   return ("Red" if col == 'R' else "Yellow") + " wins"
            if col == '.':
                game_over = False
            if col == 'R':
                whos_next = "Yellow"

    if game_over:
        return "Game Over"
    else:
        return whos_next + " plays next"


