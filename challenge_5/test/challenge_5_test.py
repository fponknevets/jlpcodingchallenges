import unittest
from challenge_5.test import challenge_5_grids as grids

from challenge_5.app.challenge_5 import get_grid_status


class Connect4Tests(unittest.TestCase):

    def test_function_exists(self):
        self.assertTrue(get_grid_status(grids.empty_grid))

    def test_empty_grid_returns_red_plays_next(self):
        self.assertEqual("Red plays next", get_grid_status(grids.empty_grid))

    def test_grid_with_R_returns_yellow_plays_next(self):
        self.assertEqual("Yellow plays next", get_grid_status(grids.one_red_grid))

    def test_grid_with_Y_returns_red_plays_next(self):
        self.assertEqual("Red plays next", get_grid_status(grids.one_yellow_grid))

    def test_grid_with_vline_red_returns_red_wins(self):
        self.assertEqual("Red wins", get_grid_status(grids.red_line_vertical))

    def test_full_grid_with_no_four_lines(self):
        self.assertEqual("Game Over", get_grid_status(grids.stalemate))

    def test_full_grid_with_four_yellow(self):
        self.assertEqual("Yellow wins", get_grid_status(grids.full4y))


if __name__ == "__main__":
    unittest.main()
