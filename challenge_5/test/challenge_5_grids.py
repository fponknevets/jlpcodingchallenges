empty_grid = \
    [
        ".......",
        ".......",
        ".......",
        ".......",
        ".......",
        "......."
    ]

one_red_grid = \
    [
        ".......",
        ".......",
        ".......",
        ".......",
        ".......",
        "R......"
    ]

one_yellow_grid = \
    [
        ".......",
        ".......",
        ".......",
        ".......",
        ".......",
        "rY....."
    ]

red_line_vertical = \
    [
        ".......",
        ".......",
        "R......",
        "r......",
        "r......",
        "ryyy..."
    ]

stalemate = \
    [
        "ryryrYr",
        "ryryryr",
        "yryryry",
        "yryryry",
        "ryryryr",
        "ryryryr"
    ]

full4y = \
    [
        "ryryyYy",
        "ryryryr",
        "yryryry",
        "yryryry",
        "ryryryr",
        "ryryryr"
    ]
