import unittest
from challenge_11.app.scratch import get_todos
from challenge_11.app.scratch import get_completed_todos_by_user
from challenge_11.app.scratch import sort_todos_by_max_completed
from challenge_11.app.scratch import get_max_complete_todos
from challenge_11.app.scratch import get_users_whove_completed_max_todos

class TodosTests(unittest.TestCase):

    def test_find_max_todo_users(self):
        todos = get_todos("https://jsonplaceholder.typicode.com/todos")
        completed_todos_by_user = get_completed_todos_by_user(todos)
        sorted_completed_todos = sort_todos_by_max_completed(completed_todos_by_user)
        max_completed_todos = get_max_complete_todos(sorted_completed_todos)
        max_users = get_users_whove_completed_max_todos(sorted_completed_todos, max_completed_todos)
        self.assertEqual("5 and 10", max_users)


if __name__ == "__main__":
    unittest.main()


