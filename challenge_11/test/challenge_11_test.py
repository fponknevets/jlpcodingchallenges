from challenge_11.app.challenge_11 import obtain_list_of_beers
import unittest


class Challenge11Tests(unittest.TestCase):

    def test_can_get_list_of_pubs_in_an_area(self):
        list_of_pubs = obtain_list_of_beers('https://pubcrawlapi.appspot.com/pubcache/?uId=mike&lng=-0.141499&lat=51.496466&deg=0.003')
        print(list_of_pubs)
        self.assertTrue(type(list_of_pubs) == list)
        self.assertEqual(len(list_of_pubs), 48)


if __name__ == "__main__":
    unittest.main()