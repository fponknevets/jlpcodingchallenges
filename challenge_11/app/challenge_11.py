import requests
from pprint import pprint as pp


def _get_pubs(pubs):
    list_of_pubs = []
    for pub_id, pub in enumerate(pubs):
        list_of_pubs[pub_id] = pub.get('Name')
    return list_of_pubs


def obtain_list_of_beers(url):

    list_of_pubs = []
    resp = requests.get(url)  # Response
    json = resp.json()  # dict
    pubs = json["Pubs"]  # list
    for i, pub_str in enumerate(pubs):
        pub = dict(pub_str)  # dict
        list_of_pubs.append(pub.get("Name"))
    return list_of_pubs
