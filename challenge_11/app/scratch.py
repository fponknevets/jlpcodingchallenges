import json
import requests


def get_todos(url):
    response = requests.get(url)
    return json.loads(response.text)


def get_completed_todos_by_user(todos):
    todos_by_user = {}
    for todo in todos:
        if todo["completed"]:
            try:
                todos_by_user[todo["userId"]] += 1
            except KeyError:
                todos_by_user[todo["userId"]] = 1
    return todos_by_user


def sort_todos_by_max_completed(todos_by_user):
    return sorted(todos_by_user.items(), key=lambda x: x[1], reverse=True)


def get_max_complete_todos(top_users):
    return top_users[0][1]


def get_users_whove_completed_max_todos(top_users, max_complete):
    users = []
    for user, num_complete in top_users:
        if num_complete < max_complete:
            break
        users.append(str(user))

    return " and ".join(users)
