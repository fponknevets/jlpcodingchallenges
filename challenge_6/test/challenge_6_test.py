import unittest

from challenge_6.app.challenge_6 import my_filter

class MyFilterTests(unittest.TestCase):

    def test_returns_list_less_than_five(self):
        list_of_ints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1]

        def _less_than_five(my_input):
            return my_input < 5

        self.assertEqual(sorted(my_filter(list_of_ints, _less_than_five)), sorted([-1, 0, 1, 2, 3, 4]))

    def test_returns_list_of_only_sixes_one_six(self):
        list_of_ints=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1]

        def _is_six(my_input):
            return my_input == 6

        self.assertEqual(sorted(my_filter(list_of_ints, _is_six)), sorted([6,]))

    def test_returns_list_of_only_sixes_no_sixes(self):
        list_of_ints=[0, 1, 2, 3, 4, 5, 7, 8, 9, -1]

        def _is_six(my_input):
            return my_input == 6

        self.assertEqual(sorted(my_filter(list_of_ints, _is_six)), sorted([]))

    def test_returns_list_of_only_sixes_three_sixes(self):
        list_of_ints=[6, 0, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9, -1]

        def _is_six(my_input):
            return my_input == 6

        self.assertEqual(sorted(my_filter(list_of_ints, _is_six)), sorted([6, 6, 6]))

    def test_returns_tuple_less_than_five(self):
        tuple_of_ints = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1)

        def _less_than_five(my_input):
            return my_input < 5

        self.assertEqual(tuple(sorted(my_filter(tuple_of_ints, _less_than_five))), (-1, 0, 1, 2, 3, 4))

    def test_returns_set_less_than_five(self):
        set_of_ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1}

        def _less_than_five(my_input):
            return my_input < 5

        self.assertEqual(set(sorted(my_filter(set_of_ints, _less_than_five))), {-1, 0, 1, 2, 3, 4})

    def test_passing_an_invalid_filter_function(self):
        list_of_ints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1]

        def _invalid_function(my_input):
            return None

        self.assertEqual(sorted(my_filter(list_of_ints, _invalid_function)), sorted([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1]))


if __name__ == "__main__":
    unittest.main()


