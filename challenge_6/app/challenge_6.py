

def my_filter(my_ints, my_filter_function):

    def _valid_filter_function(f):
        return type(f(10)) is bool

    if not _valid_filter_function(my_filter_function):
        return my_ints

    if type(my_ints) is list:
        return [my_int for my_int in my_ints if my_filter_function(my_int)]

    if type(my_ints) is tuple:
        return (my_int for my_int in my_ints if my_filter_function(my_int))

    if type(my_ints) is set:
        return {my_int for my_int in my_ints if my_filter_function(my_int)}

